#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "src/binarySearchTree.h"

#define KEYS_COUNT 32

int main() {

    clock_t start, end;
    double cpu_time_used;
    start = clock();

    Node* root = NULL;
    unsigned int key;
    char value;
    for (int i = 0; i < KEYS_COUNT; i++) {
        key = rand() % 571;
        value = 'A' + rand() % 26;

        root = insertValue(&key, &value, root);
    }

    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Time taken: %f\n", cpu_time_used);

    return 0;
}
