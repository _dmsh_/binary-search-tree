//
// Created by hc on 26.01.2023.
//

#define CTEST_MAIN
#define CTEST_COLOR_OK

#include "../src/binarySearchTree.h"
#include "ctest.h"

CTEST(insertValue, inserts_value_into_empty_tree) {
    Node* root = NULL;
    unsigned int key = 5;
    root = insertValue(&key, "value", root);

    ASSERT_NOT_NULL(root);
    ASSERT_EQUAL(*root->key, 5);
    ASSERT_EQUAL_STR(root->value, "value");
    free(root);
}

CTEST(insert_value, left_subtree) {
    Node* pNode = NULL;
    unsigned int key1 = 5;
    char* value1 = "test1";
    pNode = insertValue(&key1, value1, pNode);

    unsigned int key2 = 2;
    char* value2 = "test2";
    pNode = insertValue(&key2, value2, pNode);

    ASSERT_NOT_NULL(pNode->left);
    ASSERT_EQUAL(*pNode->left->key, key2);
    ASSERT_STR("test2", pNode->left->value);

    free(pNode->left);
    free(pNode);
}

CTEST(insert_value, right_subtree) {
    Node* pNode = NULL;
    unsigned int key1 = 5;
    char* value1 = "test1";
    pNode = insertValue(&key1, value1, pNode);

    unsigned int key2 = 8;
    char* value2 = "test2";
    pNode = insertValue(&key2, value2, pNode);

    ASSERT_NOT_NULL(pNode->right);
    ASSERT_EQUAL(*pNode->right->key, key2);
    ASSERT_STR("test2", pNode->right->value);

    free(pNode->right);
    free(pNode);
}

CTEST(deleteValueTest, deleteLeafNode) {
    Node* pNode = NULL;
    unsigned int key = 5;
    unsigned int key2 = 6;
    pNode = insertValue(&key, "A", pNode);
    pNode = insertValue(&key2, "B", pNode);

    deleteValue(&key, pNode);

    char *result = searchValue(&key, pNode);
    ASSERT_NULL(result);
}

CTEST(deleteValueTest, deleteNodeWithOneChild) {
    Node* pNode = NULL;
    unsigned int key1 = 5;
    unsigned int key2 = 6;
    unsigned int key3 = 7;
    pNode = insertValue(&key1, "A", pNode);
    pNode = insertValue(&key2, "B", pNode);
    pNode = insertValue(&key3, "C", pNode);

    deleteValue(&key2, pNode);

    char *result = searchValue(&key2, pNode);
    ASSERT_NULL(result);

    char *result2 = searchValue(&key3, pNode);
    ASSERT_EQUAL_STR(result2, "C");
}

CTEST(deleteValueTest, deleteNodeWithTwoChildren) {
    Node* root = NULL;
    unsigned int key1 = 8;
    unsigned int key2 = 3;
    unsigned int key3 = 1;
    unsigned int key4 = 6;
    unsigned int key5 = 4;
    unsigned int key6 = 7;

    root = insertValue(&key1, "A", root);
    root = insertValue(&key2, "B", root);
    root = insertValue(&key3, "C", root);
    root = insertValue(&key4, "D", root);
    root = insertValue(&key5, "E", root);
    root = insertValue(&key6, "F", root);

    deleteValue(&key2, root);

    char* deletedValue = searchValue(&key2, root);
    ASSERT_NULL(deletedValue);

    char* replacedValue = searchValue(&key5, root);
    ASSERT_EQUAL_STR(replacedValue, "E");

    root = insertValue(&key2, "B", root);

    char* insertedAgain = searchValue(&key2, root);
    ASSERT_EQUAL_STR(insertedAgain, "B");
}

int main(int argc, const char *argv[])
{
    int result = ctest_main(argc, argv);

    return result;
}