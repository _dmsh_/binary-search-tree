//
// Created by hc on 24.01.2023.
//

#ifndef BINARYSEARCHTREE_H
#define BINARYSEARCHTREE_H

typedef struct Node Node;

struct Node {
    unsigned int* key;
    char* value;
    struct Node* left;
    struct Node* right;
};

char* searchValue(unsigned int* key, Node* node);
Node* insertValue(unsigned int* key, char* value, Node* pNode);
void deleteValue(unsigned int* key, Node* pNode);

#endif //BINARYSEARCHTREE_H