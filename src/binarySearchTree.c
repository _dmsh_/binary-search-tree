//
// Created by hc on 24.01.2023.
//
#include "binarySearchTree.h"

#include <stdlib.h>
#include <string.h>

Node *initializeNode(unsigned int *key, const char *value);
Node* searchNode(unsigned int* key, Node* node);
void removeNodeWithOneChild(Node* pNode);
Node* findMinimum(Node* node);

char* searchValue(unsigned int* key, Node* node) {
    if (key == NULL)
        return NULL;
    if (node == NULL)
        return NULL;

    if (*node->key == *key)
        return node->value;

    if (*node->key > *key)
        return searchValue(key, node->left);
    else
        return searchValue(key, node->right);
}

Node* insertValue(unsigned int* key, char* value, Node* pNode) {
    if (pNode == NULL)
        return pNode = initializeNode(key, value);

    if (*pNode->key == *key)
        return pNode;

    if (*pNode->key > *key)
        if (pNode->left == NULL)
            pNode->left = initializeNode(key, value);
        else
            insertValue(key, value, pNode->left);
    else
        if (pNode->right == NULL)
            pNode->right = initializeNode(key, value);
        else
            insertValue(key, value, pNode->right);

    return pNode;
}

void deleteValue(unsigned int* key, Node* pNode) {
    Node *node = searchNode(key, pNode);
    if (node != NULL) {
        if (node->right == NULL && node->left == NULL) {
            free(node);
        } else {
            if (node->right != NULL && node->left == NULL)
                removeNodeWithOneChild(node);
            if (node->right == NULL && node->left != NULL)
                removeNodeWithOneChild(node);

            if (node->right != NULL && node->left != NULL) {
                Node* deleteNode = findMinimum(node->right);
                *node->key = *deleteNode->key;
                *node->value = *deleteNode->value;

                free(deleteNode->key);
                free(deleteNode->value);
                free(deleteNode);
            }
        }
    }
}

struct Node* initializeNode(unsigned int *key, const char *value) {
    Node* pNode = (struct Node*) malloc(sizeof (Node));
    pNode->key = (unsigned int*) malloc(sizeof (key));
    pNode->value = (char*) malloc(sizeof (value));

    stpcpy(pNode->key, key);
    strcpy(pNode->value, value);
    pNode->left = NULL;
    pNode->right = NULL;

    return pNode;
}

Node* searchNode(unsigned int* key, Node* node) {
    if (key == NULL)
        return NULL;
    if (node == NULL)
        return NULL;

    if (*node->key == *key)
        return node;

    if (*node->key > *key)
        return searchNode(key, node->left);
    else
        return searchNode(key, node->right);
}

Node* findMinimum(Node* node) {
    while (node && node->left != NULL)
        node = node->left;

    return node;
}

void removeNodeWithOneChild(Node* pNode) {
    if (pNode->right != NULL) {
        *pNode->key = *pNode->right->key;
        *pNode->value = *pNode->right->value;

        free(pNode->right->key);
        free(pNode->right->value);
        pNode->right = NULL;
    }
    if (pNode->left != NULL) {
        *pNode->key = *pNode->left->key;
        *pNode->value = *pNode->left->value;

        free(pNode->left->key);
        free(pNode->left->value);
        pNode->left = NULL;
    }
}